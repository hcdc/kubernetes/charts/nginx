
{{- define "nginx.builtinResources" -}}
limits:
  cpu: 200m
  memory: 200Mi
requests:
  cpu: 50m
  memory: 50Mi
{{- end }}
