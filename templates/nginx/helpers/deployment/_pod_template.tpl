
{{- define "nginx.podTemplate" -}}
{{- $builtinVolumes := include "nginx.builtinVolumes" . | fromYaml }}
{{- $allVolumes := mustMergeOverwrite (dict) $builtinVolumes .Values.volumes }}

metadata:
  creationTimestamp: null
  labels:
    {{- include "nginx.selectorLabels" . | nindent 4 }}
    io.kompose.service: {{ .Values.baseName }}
    access: public
  annotations:
    {{- $excludedVolumes := dict "volumes" list }}
    {{- range $name, $options := $allVolumes }}
    {{- if or $options.projected $options.excludeFromBackup }}
    {{- $_ := set $excludedVolumes "volumes" (append $excludedVolumes.volumes $name) }}
    {{- end }}
    {{- end }}
    {{- if $excludedVolumes.volumes }}
    backup.velero.io/backup-volumes-excludes: {{ join "," $excludedVolumes.volumes | quote }}
    {{- end }}

spec:
  affinity: {{- include "nginx.affinity" . | nindent 4 }}
  {{- if .Values.useManyFilesPVC }}
  securityContext:
    fsGroupChangePolicy: OnRootMismatch
    seLinuxOptions:
      type: spc_t
  serviceAccountName: manyfilespvc
  serviceAccount: manyfilespvc
  imagePullSecrets:
    - name: deployer-quay-{{ .Values.global.openshiftCluster }}
  {{- end }}
  containers:
    - name: {{ .Values.baseName }}
      {{- if .Values.command }}
      command:
        {{- range .Values.command }}
        - {{ . | quote }}
        {{- end }}
      {{- end }}
      image: '"{{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ .Values.baseName }}"'
      ports:
        - containerPort: 8080
      {{- $builtinResources := include "nginx.builtinResources" . | fromYaml }}
      {{- $mergedResources := mustMergeOverwrite (dict) $builtinResources .Values.resources }}
      resources: {{ toYaml $mergedResources | nindent 8 }}
      volumeMounts:
        {{- range $name, $options := $allVolumes }}
        {{- $defaultMountPath := print "/opt/app-root/src/" $name }}
        - mountPath: {{ $options.mountPath | default $defaultMountPath | quote }}
          name: {{ $name | quote }}
          readOnly: {{ if or (not (hasKey $options "readOnly")) (eq (coalesce $options.readOnly false) true) }}true{{ else }}false{{ end }}
        {{- end }}
  restartPolicy: Always
  volumes:
    {{- range $name, $options := $allVolumes }}
    - name: {{ $name | quote }}
      {{- if $options.projected }}
      projected: {{ toYaml $options.projected | nindent 8 }}
      {{- else }}
      persistentVolumeClaim:
        claimName: {{ $name | quote }}
      {{- end }}
    {{- end }}
{{- end }}
