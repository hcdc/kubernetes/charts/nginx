
{{- define "nginx.builtinBuildArgs" -}}
SERVER_NAME: "127.0.0.1 localhost {{ .Values.baseName }}
  {{- range $name, $options := .Values.routes }}
  {{- if $options.host }}
  {{ $options.host }}
  {{- else }}
  {{ $name }}-{{ $.Release.Namespace }}.apps.{{ $.Values.global.openshiftCluster }}.fzg.local
  {{- end }}
  {{- end }}
  "
{{- end }}
