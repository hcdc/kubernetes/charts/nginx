
{{- define "nginx.builtinBuildResources" -}}
limits:
  cpu: '1'
  memory: 1000Mi
requests:
  cpu: 50m
  memory: 50Mi
{{- end }}
