
{{- define "nginx.cronJobSpec" -}}
{{- $builtinScheduling := include "nginx.cronJobScheduling" . | fromYaml }}
{{- $mergedScheduling := mustMergeOverwrite (dict) $builtinScheduling (.scheduling | default (dict)) }}
{{ toYaml $mergedScheduling }}
{{- /* due to scoping we need to use a dict for requiredPodAffinity and override the value if necessary */}}
{{- $requiredPodAffinity := dict "isRequired" false }}
{{- if .volumes }}
  {{- range $name, $options := .volumes }}
    {{- /* Check the necessity of podAffinity */}}
    {{- if and (not $options.projected) (hasKey $options "accessModes") }}
      {{- range $options.accessModes }}
        {{- if eq . "ReadWriteOnce" }}
          {{- $_ := set $requiredPodAffinity "isRequired" true }}
        {{- end }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}

jobTemplate:
  spec:
    template:
      metadata:
        labels:
          parent: {{ .name | quote }}
        annotations:
          {{- $excludedVolumes := dict "volumes" list }}
          {{- range $name, $options := .volumes }}
          {{- if or $options.projected $options.excludeFromBackup }}
          {{- $_ := set $excludedVolumes "volumes" (append $excludedVolumes.volumes $name) }}
          {{- end }}
          {{- end }}
          {{- if $excludedVolumes.volumes }}
          backup.velero.io/backup-volumes-excludes: {{ join "," $excludedVolumes.volumes | quote }}
          {{- end }}
      spec:
        affinity:
          podAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              {{- if $requiredPodAffinity.isRequired }}
              - labelSelector:
                  matchLabels:
                    io.kompose.service: {{ $.Values.baseName }}
                    {{- include "nginx.selectorLabels" $ | nindent 20 }}
                topologyKey: kubernetes.io/hostname
              {{- end }}
        {{- if .Values.useManyFilesPVC }}
        securityContext:
          fsGroupChangePolicy: OnRootMismatch
          seLinuxOptions:
            type: spc_t
        serviceAccountName: manyfilespvc
        serviceAccount: manyfilespvc
        imagePullSecrets:
          - name: deployer-quay-{{ .Values.global.openshiftCluster }}
        {{- end }}
        containers:
          - name: {{ .name | quote }}
            {{- $builtinContainer := include "nginx.cronJobContainer" . | fromYaml }}
            {{- $mergedContainer := mustMergeOverwrite (dict) $builtinContainer (.container | default (dict)) }}
            {{- toYaml $mergedContainer | nindent 12 }}
        volumes:
        {{- range $name, $options := .volumes }}
          - name: {{ $name | quote }}
            {{- if $options.projected }}
            projected: {{ toYaml $options.projected | nindent 14 }}
            {{- else }}
            persistentVolumeClaim:
              claimName: {{ $name | quote }}
            {{- end }}
        {{- end }}
        restartPolicy: {{ .restartPolicy | default "OnFailure" | quote }}
{{- end }}
