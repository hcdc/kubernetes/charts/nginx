{{- define "nginx.cronJobContainer" -}}
image: "{{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ .Values.baseName }}"

command: {{ range (required "Specify a command to run in the cronjob" .command) }}
  - {{ . | quote }}
{{- end }}
volumeMounts:
{{- range $name, $options := .volumes }}
{{- $defaultMountPath := print "/opt/app-root/src/" $name }}
  - mountPath: {{ $options.mountPath | default $defaultMountPath | quote }}
    name: {{ $name | quote }}
    readOnly: {{ if or (not (hasKey $options "readOnly")) (eq (coalesce $options.readOnly false) true) }}true{{ else }}false{{ end }}
{{- end }}
{{- end }}
