{{- define "nginx.cronJobScheduling" -}}
schedule: "*/1 * * * *"
concurrencyPolicy: "Replace"
startingDeadlineSeconds: 200
suspend: {{ if .Values.shutdown }}true{{ else }}false{{ end }}
successfulJobsHistoryLimit: 3
failedJobsHistoryLimit: 1
{{- end }}
