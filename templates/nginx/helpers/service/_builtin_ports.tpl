
{{- define "nginx.builtinServicePorts" -}}
default:
  port: 8080
maintenance:
  port: 8081
{{- end }}
