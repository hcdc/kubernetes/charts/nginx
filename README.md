<!--
SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH

SPDX-License-Identifier: CC-BY-4.0
-->

# nginx Helm Chart


Helm Chart for deploying an application on openshift via nginx


## Installation

You need to have [helm](https://helm.sh/docs/intro/install/) installed.

First of all, login to the openshift cluster via

```bash
oc login --token=<token> --server=https://api.<openshift-host>:6443
```

and activate the project you want to work on via

```bash
oc project <project-name>
```

You then need to add the repo that includes this chart via:

```bash
CHANNEL=stable  # or latest
helm repo add  hcdc-public https://codebase.helmholtz.cloud/api/v4/projects//7205/packages/helm/${CHANNEL}
helm repo update
```

To use this chart, you can now run

```bash
helm install -f your-values.yaml nginx hcdc-public/nginx
```

You will then receive instructions how to roll it out. For further options on
customizing this chart with `your-values.yaml`, please run
`helm show values hcdc-public/nginx`
and eventually use the `-f` option instead of `--set` (see the
[limitations of --set](https://helm.sh/docs/intro/using_helm/#the-format-and-limitations-of---set)).

## Updating the chart

To update your deployment, run

```bash
helm repo update  # loads the latest info from the registry
```

and apply the update via

```bash
helm upgrade nginx hcdc-public/nginx
```


## Contact information

### Authors:
- [Philipp S. Sommer](mailto:philipp.sommer@hereon.de)


### Maintainers
- [Philipp S. Sommer](mailto:philipp.sommer@hereon.de)


## License information

Copyright © 2023 Helmholtz-Zentrum hereon GmbH


Template files in this repository is licensed under the
CC0-1.0.

Documentation files in this repository is licensed under
CC-BY-4.0.

Supplementary and configuration files in this repository are licensed
under CC0-1.0.

Please check the header of the individual files for more detailed
information.
